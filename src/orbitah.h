// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include <iso646.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <assert.h>
