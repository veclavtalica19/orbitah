// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "api.h"

#define SOKOL_IMPL
#include "extern/sokol/sokol_gfx.h"

#define FAST_OBJ_IMPLEMENTATION
#include "extern/fast_obj.h"

#define CGLM_USE_ANONYMOUS_STRUCT 1
#include "extern/cglm/include/cglm/struct/mat4.h"

#define SHADER_MANIFEST_IMPL_REGISTRIES
#include "src/shaders/.out/manifest.h"

#include "virtio.h"

static void *or_obj_file_open(const char *path, void *user_data) {
  (void)user_data;
  return virtfopen(path, "rb");
}

static void or_obj_file_close(void *file, void *user_data) {
  (void)user_data;
  int status = virtfclose(file);
  assert(status == 0);
}

static size_t or_obj_file_read(void *file, void *dst, size_t bytes,
                               void *user_data) {
  (void)user_data;
  return virtfread(dst, sizeof(char), bytes, file);
}

static unsigned long or_obj_file_size(void *file, void *user_data) {
  (void)user_data;
  long p;
  long n;

  p = virtftell(file);
  virtfseek(file, 0, SEEK_END);
  n = virtftell(file);
  virtfseek(file, p, SEEK_SET);

  if (n > 0)
    return (unsigned long)(n);
  else
    return 0;
}

static fastObjCallbacks or_obj_virtio_callbacks = {
    .file_open = or_obj_file_open,
    .file_close = or_obj_file_close,
    .file_read = or_obj_file_read,
    .file_size = or_obj_file_size,
};

bool or_create_mesh_from_file(or_Mesh *result, const char *path) {
  assert(path and result);
  result->handle =
      fast_obj_read_with_callbacks(path, &or_obj_virtio_callbacks, NULL);
  return true;
}

void or_destroy_mesh(or_Mesh *mesh) {
  assert(mesh);
  fast_obj_destroy(mesh->handle);
}

bool or_create_shader_from_precompiled_program(or_Shader *result,
                                               const char *name) {
  assert(result and name);
  for (size_t i = 0; i < SHADER_MANIFEST_SHADER_COUNT; ++i) {
    if (strcmp(name, or_shader_name_registry[i]) == 0) {
      *result =
          (or_Shader){.handle = or_shader_desc_registry[i](sg_query_backend())};
      return true;
    }
  }
  return false;
}

void or_destroy_shader(or_Mesh *mesh) {
  // todo:
}

// todo: populate buffer appropriate for shader program
bool or_create_renderable_from_mesh(or_Renderable *result, or_Mesh *mesh,
                                    or_Shader *shader) {
  assert(result and mesh and shader);

  sg_buffer vbuf = sg_make_buffer(&(sg_buffer_desc){
      .data = (sg_range){.ptr = test_mesh->positions,
                         .size = test_mesh->position_count * sizeof(float) * 3},
      .type = SG_BUFFERTYPE_VERTEXBUFFER,
  });

  // sg_buffer ibuf = sg_make_buffer(&(sg_buffer_desc){
  //     .data = (sg_range){.ptr = test_mesh->indices,
  //                        .size = test_mesh->index_count * sizeof(uint32_t) *
  //                        3},
  //     .type = SG_BUFFERTYPE_INDEXBUFFER,
  // });

  sg_shader shd = sg_make_shader(test_shader_desc(sg_query_backend()));

  pipeline = sg_make_pipeline(&(sg_pipeline_desc){
      .layout = {.buffers[0].stride = 12,
                 .attrs = {[ATTR_vs_position].format = SG_VERTEXFORMAT_FLOAT3}},
      .shader = shd,
      // .index_type = SG_INDEXTYPE_UINT32,
      .cull_mode = SG_CULLMODE_BACK,
      .depth =
          {
              .write_enabled = true,
              .compare = SG_COMPAREFUNC_LESS_EQUAL,
          },
  });

  bindings = (sg_bindings){.vertex_buffers[0] = vbuf};

  return true;
}

void or_destroy_renderable(or_Renderable *renderable) {
  // todo: free pipeline and bindings too
  free(renderable->owned_data);
}
