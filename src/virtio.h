// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// drop-in replacement for libc io that works on embedded binary resources

#pragma once

#include <stdint.h>
// note: needed for platform SEEK_X and EOF
#include <stdio.h>

void *virtfopen(const char *filename, const char *mode);
int virtfclose(void *stream);
long int virtftell(void *stream);
int virtfseek(void *stream, long int offset, int origin);
size_t virtfread(void *buffer, size_t size, size_t count, void *stream);
