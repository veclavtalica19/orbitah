#!/usr/bin/env python
# used to compile sokol-shdc annotated glsl into c header files

from os import walk, path
from subprocess import run
from textwrap import dedent

with open("./.out/manifest.h", "w") as f:
    f.write(dedent("""
        // this file is generated automatically
        #pragma once
    """[1:]))

    programs = []

    for file in next(walk('.'))[2]:
        if file.endswith(".glsl.shdc"):
            header = path.basename(file) + ".h"
            header_path = path.join("./.out", header)
            # if sokol-shdc isnt present, - download sokol-tools-bin or compile sokol-tools
            # todo: handle other shader languages, at least for opengl backend
            run(["sokol-shdc", "--input", file, "--output", header_path, "--slang", "hlsl4"])
            f.write(f"#include \"{header}\"\n")
            with open(file, "r") as p:
                p_content = p.read()
            start = p_content.find("@program")
            while True:
                start += len("@program ")
                programs.append(p_content[start:start+p_content[start:].find(" ")])
                n = p_content[start:].find("@program")
                if n == -1:
                    break
                start += n
            print(f"-- Generated {header}")

    programnames = ", ".join(map(lambda x: f"\"{x}\"", programs))
    # todo: handle namespacing in .shdc files
    programdescs = ", ".join(map(lambda x: f"{x}_shader_desc", programs))

    f.write(dedent(f"""
        #define SHADER_MANIFEST_SHADER_COUNT {int(len(programs))}
        #ifdef SHADER_MANIFEST_IMPL_REGISTRIES
        static const char *const or_shader_name_registry[] = {{{programnames}}};
        static const sg_shader_desc *(*or_shader_desc_registry[])(sg_backend) = {{{programdescs}}};
        #endif // #ifdef SHADER_MANIFEST_IMPL_REGISTRIES
    """[1:]))


print("-- Generated shader manifest.h")
