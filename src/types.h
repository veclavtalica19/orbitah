// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "extern/fast_obj.h"

#ifdef OR_BACKEND_SOKOL
#include "extern/sokol/sokol_gfx.h"

typedef struct {
  fastObjMesh *handle;
} or_Mesh;

typedef struct {
  const sg_shader_desc *handle;
} or_Shader;

// todo: store buffers to free on destroying
typedef struct {
  sg_bindings bindings;
  sg_pipeline pipeline;
  // packed vertex and indicies buffers
  void *owned_data;
} or_Renderable;

#else
#error "no backend specified"
#endif // #ifdef BACKEND_SOKOL
