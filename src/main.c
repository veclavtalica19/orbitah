// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include "orbitah.h"

#include "extern/libxm/include/xm.h"

#define SOKOL_IMPL
#include "extern/sokol/sokol_app.h"
#include "extern/sokol/sokol_audio.h"
#include "extern/sokol/sokol_glue.h"
#undef SOKOL_IMPL
#include "extern/sokol/sokol_gfx.h"

#include "extern/fast_obj.h"

#include "res/.out/manifest.h"

#define tracker_bitrate (48000U)

static xm_context_t *tracker_context;
static fastObjMesh *test_mesh;

static sg_pipeline pipeline;
static sg_bindings bindings;

static void stream_cb(float *buffer, int num_frames, int num_channels) {
  assert(tracker_context and num_channels == 2);
  xm_generate_samples(tracker_context, buffer, (size_t)num_frames);
}

static void init(void) {

  int status = xm_create_context_safe(&tracker_context, mod46_xm_data,
                                      mod46_xm_size, tracker_bitrate) == 0;
  assert(status);

  saudio_setup(&(saudio_desc){.stream_cb = stream_cb,
                              .sample_rate = tracker_bitrate,
                              .num_channels = 2});
  sg_setup(&(sg_desc){.context = sapp_sgcontext()});
}

static void frame(void) {
  sg_begin_default_pass(
      &(sg_pass_action){.colors[0] = {.action = SG_ACTION_CLEAR,
                                      .value = {1.0f, 0.0f, 0.0f, 1.0f}}},
      sapp_width(), sapp_height());
  sg_apply_pipeline(pipeline);
  sg_apply_bindings(&bindings);

  // mat4s vs_params = GLMS_MAT4_IDENTITY_INIT;
  // vs_params = glms_mat4_scale(vs_params, 5.0);

  // sg_apply_uniforms(SG_SHADERSTAGE_VS, SLOT_vs_params, &SG_RANGE(vs_params));
  sg_draw(0, test_mesh->index_count, 1);
  sg_end_pass();
  sg_commit();
}

static void cleanup(void) {
  if (test_mesh)
    fast_obj_destroy(test_mesh);
  if (tracker_context)
    xm_free_context(tracker_context);
  sg_shutdown();
  saudio_shutdown();
}

sapp_desc sokol_main(int argc, char *argv[]) {
  (void)argc;
  (void)argv;
  return (sapp_desc){
      .init_cb = init,
      .frame_cb = frame,
      .cleanup_cb = cleanup,
      .width = 640,
      .height = 480,
      .window_title = "orbitah",
      .win32_console_create = true,
      .win32_console_attach = true,
  };
}
