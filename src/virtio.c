// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// drop-in replacement for libc file io that works on embedded binary resources

#include "virtio.h"
#include "orbitah.h"

#include <stdlib.h>
#include <string.h>

#define RES_MANIFEST_IMPL_REGISTRIES
#include "res/.out/manifest.h"

struct VFILE {
  size_t file_idx;
  size_t cursor;
};

static ssize_t find_file_idx(const char *filename) {
  for (size_t i = 0; i < RES_MANIFEST_FILE_COUNT; ++i) {
    if (strcmp(filename, or_file_name_registry[i]) == 0)
      return (size_t)i;
  }
  return -1;
}

static size_t file_size(size_t idx) {
  assert(idx < RES_MANIFEST_FILE_COUNT);
  return or_file_end_registry[idx] - or_file_start_registry[idx];
}

void *virtfopen(const char *filename, const char *mode) {
  if (not filename)
    return NULL;
  ssize_t idx = find_file_idx(filename);
  if (idx == -1)
    return NULL;
  if (not mode or mode[0] != 'r')
    return NULL;
  struct VFILE *file = malloc(sizeof(struct VFILE));
  if (not file)
    return NULL;
  file->file_idx = (size_t)idx;
  file->cursor = 0;
  return (void *)file;
}

int virtfclose(void *stream) {
  if (not stream)
    return EOF;
  free(stream);
  return 0;
}

long int virtftell(void *stream) {
  if (not stream)
    return -1L;
  struct VFILE *file = (struct VFILE *)stream;
  return (long int)file->cursor;
}

int virtfseek(void *stream, long int offset, int origin) {
  if (not stream)
    return -1;
  struct VFILE *file = (struct VFILE *)stream;
  assert(file->file_idx < RES_MANIFEST_FILE_COUNT);
  if (origin == SEEK_SET) {
    file->cursor = (size_t)offset;
  } else if (origin == SEEK_CUR) {
    file->cursor = file->cursor + (size_t)offset;
    if (file->cursor >= file_size(file->file_idx))
      file->cursor = file_size(file->file_idx) - 1;
  } else if (origin == SEEK_END) {
    if (file->cursor < (size_t)offset)
      file->cursor = 0;
    else
      file->cursor = file->cursor - (size_t)offset;
  } else
    return -1;
  return 0;
}

size_t virtfread(void *buffer, size_t size, size_t count, void *stream) {
  if (not stream)
    return 0;
  // todo: Allow sizes different from single byte
  if (size != 1)
    return 0;
  struct VFILE *file = (struct VFILE *)stream;
  size_t left = file_size(file->file_idx) - file->cursor;
  size_t to_read = count > left ? left : count;
  memcpy(buffer, or_file_start_registry[file->file_idx] + file->cursor, to_read);
  file->cursor += to_read;
  return to_read;
}
