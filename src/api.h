// Copyright (C) 2022 Veclav Talica at veclavtalica@protonmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#pragma once

#include "orbitah.h"
#include "types.h"

bool or_create_mesh_from_file(or_Mesh *result, const char *path);
void or_destroy_mesh(or_Mesh *mesh);
bool or_create_shader_from_precompiled_program(or_Shader *result,
                                               const char *name);
void or_destroy_shader(or_Mesh *mesh);
bool or_create_renderable_from_mesh(or_Renderable *result, or_Mesh *mesh,
                                    or_Shader *shader);
void or_destroy_renderable(or_Renderable *renderable);
