#!/usr/bin/env python
# used to pack binary resources into linkable objects

from os import walk, path
from subprocess import run
from textwrap import dedent

virtual_files = {}

for file in next(walk("."))[2]:
    if file in {"make.py"}:
        continue
    basename = path.basename(file)
    virtual_files[basename] = { "symbol": basename.replace(".", "_") }


with open("./.out/manifest.h", "w") as f:
    f.write(dedent("""
        // this file is generated automatically
        #pragma once
        #include <stdint.h>
    """[1:]))

    for file in virtual_files:
        sym = virtual_files[file]["symbol"]
        out = path.join("./.out", file + ".o")
        run(["ld", "-r", "-b", "binary", "-o", out, file])
        # todo: is char type appropriate?
        f.write(f"extern const char binary_{sym}_start[];\n")
        f.write(f"extern const char binary_{sym}_end[];\n")
        f.write(f"#define {sym}_data (const char *const)(binary_{sym}_start)\n")
        f.write(f"#define {sym}_size (size_t)(binary_{sym}_end - binary_{sym}_start)\n")

    filenames = ", ".join(map(lambda x: f"\"{x}\"", virtual_files))
    filestarts = ", ".join(map(lambda x: f"binary_{virtual_files[x]['symbol']}_start", virtual_files))
    fileends = ", ".join(map(lambda x: f"binary_{virtual_files[x]['symbol']}_end", virtual_files))

    f.write(dedent(f"""
        #define RES_MANIFEST_FILE_COUNT {int(len(virtual_files))}
        #ifdef RES_MANIFEST_IMPL_REGISTRIES
        static const char *const or_file_name_registry[] = {{{filenames}}};
        static const char *const or_file_start_registry[] = {{{filestarts}}};
        static const char *const or_file_end_registry[] = {{{fileends}}};
        #endif // #ifdef RES_MANIFEST_IMPL_REGISTRIES
    """[1:]))


print("-- Generated resource manifest.h")
