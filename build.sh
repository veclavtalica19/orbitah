#!/usr/bin/env bash
# building orchestration script

do_build=false
do_clean=false
pack_resources=false
compile_shaders=false
build_type=Debug

for arg in "$@"
do
    case $arg in
        release)
            do_build=true
            build_type=Release
            ;;
        debug)
            do_build=true
            build_type=Debug
            ;;
        pack_resources)
            pack_resources=true
            ;;
        compile_shaders)
            compile_shaders=true
            ;;
        clean)
            do_clean=true
            ;;
        all)
            build_tools=true
            pack_resources=true
            compile_shaders=true
            ;;
    esac
done

if $do_clean; then
    rm -f ./res/.out/*
    rm -f ./src/shaders/.out/*
    rm -fr ./.build/*
fi

if $pack_resources; then
    (cd ./res && ./make.py)
fi

if $compile_shaders; then
    if ! command -v sokol-shdc &> /dev/null; then
        echo 'sokol-shdc isnt present, - download sokol-tools-bin or compile sokol-tools'
        exit
    fi
    (cd ./src/shaders && ./make.py)
fi

if $do_build; then
    # todo: use something else if ninja isnt available
    cmake -G "Ninja" -B ./.build -DCMAKE_BUILD_TYPE=$build_type && cmake --build ./.build
fi
